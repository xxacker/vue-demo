import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

/**
 * 路由文件
 * @param path  文件路径
 * @param file  文件名
 * @param name  文件名称
 * @param children   子路由
 * @returns {{path: *, component: (function()), children: *, exact: boolean, name: *}}
 */
function route (path, file, name, children) {
  return {
    exact: true,
    path,
    name,
    children,
    component: () => import('../pages' + file)
  }
}

export default new Router({
  routes: [
    route("/login",'/Login',"Login"),        // /login 路径，路由到登录组件【待完善...】
    {
      path:"/",                                               // 1. 根路径，路由到 Layout 组件【项目的根域名，会触发该路由】
      component: () => import('../pages/Layout'),             // 2. 导入该组件
      redirect:"/index/dashboard",                            // ** 访问首页直接显示的界面 【重定向页面访问路径】
      children:[                                              // 其它所有组件都是 Layout 的子组件
        route("/index/dashboard","/Dashboard","Dashboard"),   // 仪表盘页面
        route("/item/category",'/item/Category',"Category"),  // 种类列表页面
        route("/item/brand",'/item/Brand',"Brand"),           // 品牌页面
        route("/item/list",'/item/Goods',"Goods"),            // 商品页面
        route("/item/specification",'/item/specification/Specification',"Specification"),   // 规格页面
        route("/user/statistics",'/item/Statistics',"Statistics"),    // 统计表页面
        route("/trade/promotion",'/trade/Promotion',"Promotion")      // 促销页面
      ]
    }
  ]
})
