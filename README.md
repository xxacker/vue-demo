# 淼森商城 - 后台管理系统

#### 介绍
该项目是淼森商城后台管理系统的前端项目，基于 Vue+Element 实现。包括系统统计、商品管理、订单管理、会员管理、销售管理、权限管理、系统管理、促销管理、财务管理、统计报表等功能。

#### 演示界面
![home](/images/home.png)

![category](images/category.png)

![brand](./images/brand.png)

![add_brand](/images/add_brand.png)

![goods](images/goods.png)

![add_goods](/images/add_goods.png)

![parameters](/images/parameters.png)


#### 项目架构

![framework](/images/framework.png)

#### 功能模块图

![Function-module-diagram](/images/Function-module-diagram.png)

## 参考资料

- [Spring实战（第4版）](https://book.douban.com/subject/26767354/)
- [Spring Boot实战](https://book.douban.com/subject/26857423/)
- [Spring Cloud微服务实战](https://book.douban.com/subject/27025912/)
- [Spring Cloud与Docker微服务架构实战](https://book.douban.com/subject/27028228/)
- [Spring Data实战](https://book.douban.com/subject/25975186/)
- [MyBatis从入门到精通](https://book.douban.com/subject/27074809/)
- [深入浅出MySQL](https://book.douban.com/subject/25817684/)
- [循序渐进Linux（第2版）](https://book.douban.com/subject/26758194/)
- [Elasticsearch 技术解析与实战](https://book.douban.com/subject/26967826/)
- [MongoDB实战(第二版)](https://book.douban.com/subject/27061123/)
- [Kubernetes权威指南](https://book.douban.com/subject/26902153/)







